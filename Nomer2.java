package JavaLabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 08.02.2016.
 * Вывод на экаран прямоугольника из восьмёрок размерности a*b
 */
public class Nomer2 {
    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите два целых числа");
        String a1 = buf.readLine();
        String b1 = buf.readLine();
        try {
            Integer.parseInt(a1);
            Integer.parseInt(b1);
        } catch (Exception e) {
            System.out.println("Введено не число");
            return;
        }
        int a = Integer.parseInt(a1);
        int b = Integer.parseInt(b1);
        for (int i = 0; i < a; i++) {
            System.out.println();
            for (int j = 0; j < b; j++) {
                System.out.print(8);
            }
        }


    }
}
