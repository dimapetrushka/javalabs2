package JavaLabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.String;

/**
 * Created by Dima1 on 09.02.2016.
 * Сравнить два имени, введёных с клавиатуры, по длине или на идентичность
 */
public class Nomer1up {
    public static void main(String[] args) throws IOException {
        int j = 0;
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        String a1 = buf.readLine(), b1 = buf.readLine();
        String a = a1.toLowerCase(), b = b1.toLowerCase();
        if (a1.equals(b1))
            System.out.print("Имена идентичны");
        else {
            if (a1.length() == b1.length())
                System.out.print("Длины имен равны");
        }
    }
}



