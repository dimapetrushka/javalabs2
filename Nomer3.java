package JavaLabs;

/**
 * Created by Dima1 on 08.02.2016.
 * Вывести треугольник из восьмёрок размерности 10*10
 */
public class Nomer3 {
    public static void main(String[] args) {
        for (int i = 1; i < 11; i++) {
            System.out.println();
            for (int j = 0; j < i; j++) {
                System.out.print(8);
            }
        }
    }
}
