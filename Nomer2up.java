package JavaLabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 08.02.2016.
 * Нахождение минимума среди четырёх целых чисел с клавиатуры, через вызов функции min
 */
public class Nomer2up {
    //Функция нахождения минимума среди двух чисел
    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else return b;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите 4 целых числа");
        String a1 = buf.readLine(), b1 = buf.readLine(), c1 = buf.readLine(), d1 = buf.readLine();
        //проверка на исключения
        try {
            Integer.parseInt(a1);
            Integer.parseInt(b1);
            Integer.parseInt(c1);
            Integer.parseInt(d1);
        } catch (Exception e) {
            System.out.println("Введено не int");
            return;
        }
        int a = Integer.parseInt(a1), b = Integer.parseInt(b1), c = Integer.parseInt(c1), d = Integer.parseInt(d1);
        System.out.println("Минимальное среди введёных чисел =>" + min(min(a, b), min(c, d)));
    }
}
