package JavaLabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 08.02.2016.
 * Определение четверти
 */
public class Nomer3up {
    public static void main(String[] args) throws IOException {
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите координаты(не 0!)");
        String a1 = buf.readLine(), b1 = buf.readLine();
        try {
            Integer.parseInt(a1);
            Integer.parseInt(b1);
        } catch (Exception e) {
            System.out.println("Введено не int");
            return;
        }
        int a = Integer.parseInt(a1), b = Integer.parseInt(b1);
        if (a == 0 || b == 0) {
            System.out.println("Числа не должны быть равны нулю");
        } else {
            if (a > 0 && b > 0) System.out.println("Первая четверть");
            else if (a > 0 && b < 0) System.out.println("Четвёртая четверть");
            else if (a < 0 && b > 0) System.out.println("Вторая четверть");
            else System.out.println("Третья четверть");
        }
    }
}
