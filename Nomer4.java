package JavaLabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 08.02.2016.
 * Нахождение минимума среди двух чисел с клавиатуры в отдельной функции
 */
public class Nomer4 {
    //Функция нахождения минимума среди двух чисел
    public static void min(int a, int b) {
        if (a != b) {
            if (a < b) {
                System.out.println(a + " меньше, чем " + b);
            } else {
                System.out.println(b + " меньше, чем " + a);
            }
        } else {
            System.out.println("Введено 2 одинаковых числа");
        }
    }

    public static void main(String[] args) throws IOException {

        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите два целых числа");
        String a1 = buf.readLine();
        String b1 = buf.readLine();
        //проверка на исключения
        try {
            Integer.parseInt(a1);
            Integer.parseInt(b1);
        } catch (Exception e) {
            System.out.println("Введено не int");
            return;
        }
        int a = Integer.parseInt(a1);
        int b = Integer.parseInt(b1);
        min(a, b);

    }
}
